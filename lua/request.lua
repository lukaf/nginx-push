package.path = package.path .. ";lib/?.lua"

require 'utils'

local query = ngx.var.uri .. ngx.var.is_args .. ngx.var.args

local function check_uri_and_rewrite(query)
    r = utils.split(query, "+")
    local channels = r[1]
    local oauth = r[2]
    res = ngx.location.capture("/oauth/check" .. oauth)
    if res.status == 200 then
        ngx.req.set_uri(channels, true)
    else
        ngx.req.set_uri("/denied", true)
    end
end
