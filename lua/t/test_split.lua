package.path = package.path .. ";../lib/?.lua"

require "utils"

local delimiters = {
    "\'", "!", "\"", "#", "$", "%%", "&", "(", ")", "*", "+", ",", "-", ".",
    --[["/",]] ":", ";", "<", "=", ">", "?", "@", "[", "\\", "]", "^",
    "_", "`", "{", "|", "}", "~"
}

local str1 = "/begin/url"
local str2 = "arg1"
local str3 = "arg2"
local str4 = "arg3"

local str = {str1, str2, str3, str4}
local d = ""

for _, del in pairs(delimiters) do
    s = str1 .. del .. str2 .. del .. str3 .. del .. str4
    split_return = utils.split(s, del)
    assert(#str == #split_return, "[FAILED] number of elements do not match: expected " .. #str .. " got " .. #split_return .. " when using " .. del)
    for n, elem in pairs(str) do
        assert(str[n] == split_return[n], "[FAILED] strings do not match: expected " .. str[n] .. " got " .. split_return[n] .. " when using " .. del)
    end
end

print("[OK] " .. #delimiters .. " delimiters checked (" .. table.concat(delimiters) .. ")")
