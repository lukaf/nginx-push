local function split(str, delimiter)
    local splitted = {}
    delimiter = "[^" .. delimiter .. "]+"
    for w in string.gmatch(str, delimiter) do
        splitted[#splitted + 1] = w
    end
    -- return splitted
    return unpack(splitted)
end

local function len(x)
    if (type(x) == "table") then
        return #x
    elseif (type(x) == "string") then
        return string.len(x)
    else
        return nil
    end
end

utils = {
    split = split,
    len = len
}
