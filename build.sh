#!/bin/sh

base=`pwd`

cpu=2
if [ "`uname -s`" = "Linux" ]; then
    cpu=`awk '/processor/{cpu = $NF} END{printf("%d", cpu + 1)}' /proc/cpuinfo`
fi

luajit_version="2.0.0"
nginx_lua_version="0.8.1"
nginx_push_version="0.3.5"

luajit_install_path="${base}/luajit-${luajit_version}"

# Check if the folder under "include" has changed when switching to other
# LuaJIT versions!
export LUAJIT_INC=${LUAJIT_INC:=${luajit_install_path}/include/luajit-2.0}
export LUAJIT_LIB=${LUAJIT_LIB:=${luajit_install_path}/lib}

luajit="http://luajit.org/download/LuaJIT-${luajit_version}.tar.gz"
nginx_lua="https://nodeload.github.com/chaoslawful/lua-nginx-module/tar.gz/v${nginx_lua_version}"
nginx_push="https://nodeload.github.com/wandenberg/nginx-push-stream-module/tar.gz/${nginx_push_version}"

luajit_path="LuaJIT-${luajit_version}"
nginx_lua_path="lua-nginx-module-${nginx_lua_version}"
nginx_push_path="nginx-push-stream-module-${nginx_push_version}"

modules="\
--add-module=../${nginx_lua_path} \
--add-module=../${nginx_push_path} \
--with-http_ssl_module \
--with-http_stub_status_module \
--with-http_gzip_static_module \
--with-ipv6"

log() {
    echo
    echo "### ${@}"
}

info() {
    echo "### ${@}"
}

warn() {
    echo "WARN: ${@}"
}


die() {
    echo
    echo "ERROR: ${@}"
    exit 1
}

# download and extract
dae() {
    what=${1}
    what_path=${2}
    if [ -z "${what}" -o -z "${what_path}" ]; then
        die "dae expects exactly two arguments!"
    else
        log "Downloading and extracting ${what}"
        if [ -d "${what_path}" ]; then
            info "... skipping, ${what_path} already exists."
        else
            curl --insecure -s ${what} | tar xvzf - || \
                die "unable to fetch or extract ${what}"
        fi
    fi
}

if [ -z "${1}" ]; then
    echo "Usage: ${0##*/} nginx-version"
    echo "Example: ${0##*/} 1.2.6"
    exit 1
else
    nginx_version=$1
    shift
fi

# Go to base.
cd ${base} || die "unable to change to base dir: ${base}"

# Download modules.
dae ${nginx_lua} ${nginx_lua_path}
dae ${nginx_push} ${nginx_push_path}
dae ${luajit} ${luajit_path}

# Build and install LuaJIT.
log "Building and installing LuaJIT-${luajit_version} ..."
(cd ${luajit_path} && make -j ${cpu} && make -j ${cpu} install PREFIX=${luajit_install_path}) || \
    die "failed to install LuaJIT-${luajit_version}"

# Download nginx.
dae http://nginx.org/download/nginx-${nginx_version}.tar.gz nginx-${nginx_version}

# Build nginx.
log "Building nginx-${nginx_version} ..."
(cd nginx-${nginx_version} && \
    ./configure --with-ld-opt="-Wl,-rpath,${LUAJIT_LIB}" ${modules} --prefix=${base} $@ && \
    make -j ${cpu}) || die "failed to build nginx-${nginx_version}"

# Damn we're good!
log "Nginx ${nginx_version} built!"
log "here is some info, just for you ..."
nginx-${nginx_version}/objs/nginx -V 2>&1 | sed 's/--/\n/g'

echo
warn "############################################################################################################################"
warn "If you receive an error while loading shared libraries (probably regarding the LuaJIT), set the LD_LIBRARY_PATH accordingly."
warn "Example: export LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:${luajit_install_path}/lib"
warn "Or copy the required file under one of the standard library paths."
warn "############################################################################################################################"

log "New nginx binary location: nginx-${nginx_version}/objs/nginx"

