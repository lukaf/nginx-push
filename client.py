#
# When creating more than 1000 client, some system settings must be changed.
# Raise the number of max open file descriptors:
#   ulimit
#   /etc/security/limits.conf
#
# Raise the port range:
#   net.ipv4.ip_local_port_range
#
# If using iptables, raise the number of connections that connection tracking
# can handle:
#   net.nf_conntrack_max
#
#   ulimit -n 999999 (limits.conf: root nofile 999999)
#   sysctl net.ipv4.ip_local_port_range="1024 65535"
#   sysctl net.nf_conntrack_max=999999
#

from __future__ import print_function

import socket
import asyncore
import sys
import getopt
import signal
import os


class Client(asyncore.dispatcher):  # {{{
    def __init__(self, host, dst_port, path, client_id=0, src_ip='127.0.0.1',
        src_port=0):
        # asyncore.dispatcher uses old style classes!
        #super(Client, self).__init__()
        asyncore.dispatcher.__init__(self)
        self.host = host
        self.dst_port = dst_port
        self.path = path
        self.client_id = client_id
        self.src_ip = src_ip
        self.src_port = src_port
        self.send_data = 'GET %s HTTP/1.0\r\nHost: %s\r\n\r\n' % (
                self.path, self.host)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.src_ip, 0))
        try:
            self.connect((self.host, self.dst_port))
        except Exception as msg:
            self.info("\nsocket.connect() exception: %s (%s)\n" % (msg, self.getsockname()))
            self.handle_close()

    def info(self, msg):
        sys.stdout.write(msg)
        sys.stdout.flush()

    # Event when connection is established (first, read/write event).
    def handle_connect(self):
        self.info("\rClient %d connected to http://%s:%d%s from %s" % (
                self.client_id, self.host, self.dst_port, self.path, self.getsockname()))
        global clients
        clients.append(self._fileno)

    def handle_close(self):
        global clients
        if self._fileno in clients:
            clients.remove(self._fileno)
        self.info("Client %d closed connection, reconnecting ..." %
                self.client_id)
        self.close()
        Client(self.host, self.dst_port, self.path, client_id=self.client_id,
                src_ip=self.src_ip)

    # Data ready.
    def handle_read(self):
        data = self.recv(1024)
        if len(data) > 0:
            try:
                x = data.split('\r\n')
                if int(x[0], 16) > 89:
                    self.info("Client %d received: %s\n" % (
                        self.client_id, x[1]))
            except:
                pass

    # If writable, jump to handle_write.
    def writable(self):
        return (len(self.send_data) > 0)

    # All the data we need to send is initial GET.
    def handle_write(self):
        try:
            sent = self.send(self.send_data)
            self.send_data = self.send_data[sent:]  # }}}
        except:
            self.handle_close()

    def handle_error(self):
        t, v, tb = sys.exc_info()
        print("Client %d error: %s" % (self.client_id, v))
        sys.exc_clear()


def usage():  # {{{
    print(
    "Usage: %s -s ip[,ip,..] -h host -d dst_port -p path -c count [-o offset]"
    % sys.argv[0]
    )
    print("-s ip[,ip,ip,..]     source IP, separate with comma for many IPs")
    print("-h host              host to which we connect")
    print("-d remote port       port number to which we connect")
    print("-p path              URL path (client id is appended)")
    print("-c count             number of clients to generate")
    print("-o offset            offset for client id")
    sys.exit(1)  # }}}


def err(msg):
    print(msg)
    sys.exit(1)


def counter(signum, stack):
    print("Connected clients: %d" % len(clients))


signal.signal(signal.SIGTSTP, counter)


if __name__ == '__main__':

    clients = []

    ip_list = []
    host = None
    dst_port = None
    path = None
    count = None
    c = 0
    offset = 0

    try:  # {{{
        opts, args = getopt.getopt(sys.argv[1:], 's:h:d:p:c:o:')
    except getopt.GetoptError as err:
            print("ERROR: %s" % err)
            sys.exit(1)

    for opt, arg in opts:
        if opt == '-s':
            ip_list = arg.replace(' ', '').split(',')
        elif opt == '-h':
            host = arg
        elif opt == '-d':
            try:
                dst_port = int(arg)
            except:
                err("ERROR: argument to -d must be an int")
        elif opt == '-p':
            path = ''.join([arg.rstrip('/'), '/%s'])
        elif opt == '-c':
            try:
                count = int(arg)
            except:
                err("ERROR: argument to -c must be an int")
        elif opt == '-o':
            try:
                offset = int(arg)
            except:
                err("ERROR: argument to -o must be an int")
        else:
            print(opt)
            usage()  # }}}

    if not all([ip_list, host, dst_port, path, count]):
        usage()

    client_id = offset

    for ip in ip_list:
        for _ in xrange(count):
            client_id += 1
            #clients.append(
            #        Client(host, dst_port, path % client_id,
            #            client_id=client_id, src_ip=ip)
            #        )
            Client(host, dst_port, path % client_id, client_id=client_id,
                    src_ip=ip)

    try:
        asyncore.loop(use_poll=True)
    except KeyboardInterrupt as msg:
        print("Interrupt received, closing connections")
        for client in clients:
            os.close(client)
